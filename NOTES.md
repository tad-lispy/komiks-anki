# Braindump

This document contains various ideas and snippets that can be incorporated into README or other documents, but are not mature enough yet. It's like a scratch buffer, but with version control.


## Alternative taglines

Current tagline is: 

> "Fill in the blanks in your language" 

Previously it was:

> "Learn languages and more by filling missing words in comic books and such."

Here are some alternative takes:

  1. "Master new languages with comic book puzzles"
  
  2. "Enrich your linguistic arsenal with missing word fun"
  
  3. "Strengthen your language comprehension with missing word exercises"
  
  4. "Enhance your language knowledge through comic book practice"
  
  5. "Expand your language horizons with missing word comic books"
  
  6. "Improve how well you know words by doing exercises where some words are missing."
  
  7. "Learn new words"
  
  8. "Fill in the blanks" 


## Use cases 

What can I learn by filling in the blanks in graphic novels or posters?

The setting and environment of the story.

The dialogue between characters.

The relationships between characters.

The characters' emotions and feelings.

The cultural and historical context of the story.

The author or artist's message or commentary on society or politics.

The target audience and intended age range.

The use of humor and satire.

Expand scientific vocabulary through labelling exercises

Master technical terms through visual matching games

Practice legal terminology through case study exercises

Empower business language skills through word puzzles

Enhance medical vocabulary through anatomy matching games

Practice world geography through place name games

Master literature terms through literary device exercises

Learn math terminology through problem-solving games

Develop music literacy through note naming games

Increase financial literacy through terminology exercises

Improve digital literacy through technology vocabulary games.




## Cloze Exercise

A form of written examination in which candidates are required to provide words that have been omitted from sentences, thereby demonstrating their knowledge and comprehension of the text. 

https://en.wikipedia.org/wiki/Cloze_test

Cloze comes from "closure" in the [Gestalt theory](https://en.wikipedia.org/wiki/Gestalt_psychology).

Closure also means "The process whereby the reader of a comic book infers the sequence of events by looking at the picture panels" (after https://en.wiktionary.org/wiki/closure#Noun). 


## Ask for Help

Tekenen, schilderij


## List of topics to vote for 

- Huis
- Bezoek bij de huisarts
- In de winkel (bakker, drogist, groente winkel, boodschap, parfumerie)
- Het licham
- Kleeding
- Het weer
- Tijd
- Vervoer (train, fiets, auto, vliegtuig)
- Koken
- Bij de bibliothek
- Landen in Europa
- Hobby's
- Kinderopvang en school
- Familie
- Vakantie
- De stad and richting
- Beroepen


From Barbara, Rob, Henk, etc.:


- Dieren
- De natuur
- De markt
- Boodschappen
- Kunst
- Veluwe
- Wonen
- Bussum en omsteken
- Samen studeren
- Bij de gym


- Weet, ken of kan?
- In het restaurant
- Richting

From ChatGPT:

> Give me 40 examples of everyday situations that require knowledge of local language and may be difficult for a person living abroad.

1. Ordering food at a local restaurant
2. Asking for directions to a specific location
3. Communicating with a healthcare provider
4. Registering for school or university courses
5. Applying for a job
6. Buying groceries at a local market
7. Requesting assistance from government agencies
8. Negotiating prices with vendors
9. Understanding public transportation routes and schedules
10. Interacting with law enforcement officials
11. Attending a religious service or ceremony
12. Filling out paperwork for residency or citizenship status
13. Participating in community events or festivals
14. Renting an apartment or house
15. Enrolling children in school
16. Interacting with neighbors or community members
17. Seeking medical care for a pet
18. Conducting business transactions
19. Understanding cultural customs and social norms
20. Seeking legal representation in court
21. Registering for a gym or fitness center
22. Interacting with coworkers or superiors in a workplace environment
23. Seeking legal advice for personal matters
24. Participating in a hobby or interest group
25. Applying for a bank account or loan
26. Negotiating with landlords or property owners
27. Understanding and following traffic laws
28. Seeking assistance from a tourist information center
29. Interacting with immigration officials at the airport
30. Seeking specialized medical care
31. Filing tax returns or other legal documents
32. Interacting with utility companies for electricity, gas, or water services
33. Flying a drone for recreation or professional purposes
34. Collaborating with local artists or artisans
35. Interacting with domestic help, such as maid or housekeeper
36. Attending cultural performances or events
37. Requesting repairs for appliances or property damage
38. Seeking mental health counseling or therapy services
39. Participating in a sports team or recreational league
40. Understanding local rules and regulations for hunting or fishing.
