// This module deals with a nasty habit of mobile Safari to cover elements with
// position fixed with the virtual keyboard.

export function setup_visual_viewport_hack () {
  const viewport = window.visualViewport;
  if (!viewport) {
    console.warn ("There is no visual viewport property in the window! Aborting.")
    return
  }
  viewport.addEventListener ("resize", on_resize)
}

function on_resize (event) {
  const elements = document.querySelectorAll (".visual-fill")
  const { height } = event.target
  for (const element of elements) {
    element.style.height = `${ height }px`
  }
}
