use crate::controls::UserGuess;
use gloo_console as console;
use js_sys::Reflect;
use wasm_bindgen::{prelude::*, UnwrapThrowExt};
use web_sys::window;

// These values come from https://app.usefathom.com/sites/ZAUEWXEZ/events
const CORRECT_ANSWER_EVENT_ID: &str = "CBJHF62Q";
const WRONG_ANSWER_EVENT_ID: &str = "YTHKOYAJ";
const DOCUMENT_COMPLETED_EVENT_ID: &str = "Z4HEG3TK";

#[wasm_bindgen]
extern "C" {
    type FathomJs;

    #[wasm_bindgen(structural, method)]
    pub fn trackGoal(this: &FathomJs, event_id: &str, value: f32);
}

pub fn report_answer(answer: &UserGuess) {
    let Some(fathom) = get_js_object() else {
        console::log!("No fathom object in this window.");
        return
    };

    match answer {
        UserGuess::NoGuess => {
            console::warn!("Trying to report an guess, but there is none!");
        }
        UserGuess::Correct { .. } => fathom.trackGoal(CORRECT_ANSWER_EVENT_ID, 0.0),
        UserGuess::Incorrect { .. } => fathom.trackGoal(WRONG_ANSWER_EVENT_ID, 0.0),
    };
}

pub fn report_document_completion() {
    let Some(fathom) = get_js_object() else {
        console::log!("No fathom object in this window.");
        return
    };

    fathom.trackGoal(DOCUMENT_COMPLETED_EVENT_ID, 0.0)
}

fn get_js_object() -> Option<FathomJs> {
    let window = window().expect_throw("There should be a window here!");
    let object = match Reflect::get(&window, &JsValue::from_str("fathom")) {
        Ok(object) => object,
        Err(error) => {
            console::warn!(error);
            return None;
        }
    };
    let fathom: FathomJs = object.into();
    Some(fathom)
}
