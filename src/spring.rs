use std::time::Duration;

/// This is a Rust port of my Elm Springs package
///
/// See https://package.elm-lang.org/packages/tad-lispy/springs/latest/

// TODO: Change comments to Rust. They are copied from Elm code.

#[derive(Clone, Copy, Debug)]
pub struct Spring {
    strength: f32,
    dampness: f32,
    target: f32,
    value: f32,
    velocity: f32,
}

impl Spring {
    const MIN_SAMPLING_DURATION: Duration = Duration::new(0, 1_000_000_000 / 15); // 15 fps

    // Create a spring with the value and target set to `0`.
    //
    // Let's say we are creating a program that animates the position of an element toward last click position.
    //     type alias Model =
    //         { x : Spring
    //         , y : Spring
    //         }
    //
    //     init : Flags -> ( Model, Cmd msg )
    //     init flags =
    //         ( { x = Spring.create { strength = 20, dampness = 4.5 }
    //           , y = Spring.create { strength = 20, dampness = 4.5 }
    //           }
    //         , Cmd.none
    //         )
    //
    // Note that the `dampness` is a logarythmic value: `dampness = 5` results in damping ratio 25 times higher than `dampness = 1`.
    //
    pub fn new(strength: f32, dampness: f32) -> Spring {
        Self {
            strength,
            dampness: dampness.powi(2),
            target: 0.0,
            value: 0.0,
            velocity: 0.0,
        }
    }

    // {-| Set a new target (relaxed value) for the spring.

    // The current value and its velocity will be preserved, so the spring will smoothly transition its movement. Typically you would set a target in response to an event, e.g.:

    //     update : Msg -> Model -> ( Model, Cmd Msg )
    //     update msg model =
    //         case msg of
    //             Click x y ->
    //                 ( { model
    //                     | x = Spring.setTarget x model.x
    //                     , y = Spring.setTarget y model.y
    //                   }
    //                 , Cmd.none
    //                 )

    // -}
    // TODO: Maybe just expose the target field of the struct?
    pub fn set_target(&mut self, target: f32) -> &Self {
        self.target = target;
        self
    }

    // Gently update the internal state of the spring

    // Typically you would do it in response to an animation frame message, like this:
    //
    //     subscriptions : Model -> Sub Msg
    //     subscriptions model =
    //         Browser.Events.onAnimationFrameDelta Animate
    //
    //     update : Msg -> Model -> ( Model, Cmd Msg )
    //     update msg model =
    //         case msg of
    //             Animate delta ->
    //                 ( { model
    //                     | x = Spring.animate delta model.x
    //                     , y = Spring.animate delta model.y
    //                   }
    //                 , Cmd.none
    //                 )
    //
    // > Note: that if the frame rate is low, the time passage experienced by the spring will deviate from the real time. See the comment in source code for more details.

    pub fn animate(&mut self, delta: &Duration) -> &Self {
        // If it's in equilibrium, then let's just skip the whole calculation. Be lazy.
        if self.is_at_rest() {
            return self;
        }

        if ((self.value - self.target).abs() < 0.05) && (self.velocity.abs() < 0.05) {
            // In reality the spring never stops vibrating, but at some point the vibration is lost in the background noise. In our case it's also a wasted computation. Let's just say that it is at rest already.
            return self.jump_to(self.target);
        }

        // Low sampling rates often result in nasty errors.
        //
        // With this cap on delta, if the frame rate is lower than 30fps the animation will slow down and good sampling rate will be preserved.
        //
        // TODO: Consider simulating multiple steps by splitting duration into smaller deltas and running them in a loop. Maybe as an option?
        let delta = delta.min(&Self::MIN_SAMPLING_DURATION);
        let seconds = delta.as_secs_f32();
        let stretch = self.target - self.value;
        let force = stretch * self.strength;
        let damping = self.velocity * self.dampness;
        let acceleration = seconds * (force - damping);

        self.velocity += acceleration;
        self.value += self.velocity * seconds;
        self
    }

    // {-| Measure the current value of the spring.

    // Typically you want to access it in the view function, like this:

    //     Element.el
    //         [ Element.width (Element.px 20)
    //         , Element.height (Element.px 20)
    //         , Font.size 30
    //         , model.x
    //             |> Spring.value
    //             |> Element.moveRight
    //         , model.y
    //             |> Spring.value
    //             |> Element.moveDown
    //         ]
    //         (Element.text "\u{1F991}")

    // > Above I use [Elm UI](https://package.elm-lang.org/packages/mdgriffith/elm-ui/latest/) Elements and Attributes, but it's not difficult to implement the same behaviour using CSS transformations. The value of a Spring is just a `f32` - you can do with it whatever you need.

    // -}
    pub fn get_value(&self) -> f32 {
        self.value
    }

    // {-| Get current target of a spring

    // Can be useful to see where the spring is going. Maybe you want to display something there?

    // -}
    pub fn get_target(&self) -> f32 {
        self.target
    }

    // {-| Check if the spring is at rest

    // It indicates that the spring has reached its target and no motion is going on. Maybe you want to unsubscribe from animation frames? Or remove an element?

    //     subscriptions : Model -> Sub Msg
    //     subscriptions model =
    //         if Spring.atRest model.x && Spring.atRest model.y then
    //             Sub.none

    //         else
    //             Browser.Events.onAnimationFrameDelta Animate

    // -}
    pub fn is_at_rest(&self) -> bool {
        self.value == self.target && self.velocity == 0.0
    }

    // {-| Forcefully set the value and interrupt the motion.

    // The target will be preserved, but velocity will be set to 0. It is useful when you set the spring for the first time (e.g. in the `init` function) or you want to reset the animation.

    //     init : Flags -> ( Model, Cmd msg )
    //     init flags =
    //         ( { x =
    //                 Spring.create 20 20
    //                     |> Spring.setTarget 200
    //                     |> Spring.jumpTo 200
    //           , y =
    //                 Spring.create 20 20
    //                     |> Spring.setTarget 200
    //                     |> Spring.jumpTo 200
    //           }
    //         , Cmd.none
    //         )

    // -}
    pub fn jump_to(&mut self, value: f32) -> &Self {
        self.value = value;
        self.velocity = 0.0;
        self
    }
}

#[cfg(test)]
mod spring_tests {
    use std::ops::AddAssign;

    use super::*;

    struct TestSetup {
        strength: f32,
        dampness: f32,
        target: f32,
        seconds_too_short: f32,
        seconds_too_long: f32,
        fps: f32,
    }

    // Compare with https://tad-lispy.gitlab.io/elm-springs/Oscillator.html
    fn test_with(setup: TestSetup) {
        let mut spring = Spring::new(setup.strength, setup.dampness);
        spring.set_target(setup.target);
        let too_long = Duration::from_secs_f32(setup.seconds_too_long);
        let too_short = Duration::from_secs_f32(setup.seconds_too_short);

        let delta = Duration::from_secs_f32(1.0 / setup.fps);
        let mut elapsed = Duration::new(0, 0);
        loop {
            println!("{elapsed:?}: {spring:?}");
            spring.animate(&delta);
            elapsed.add_assign(delta);

            assert!(
                elapsed < too_long,
                "It took too long! {elapsed:?} ≥ {too_long:?} "
            );

            if spring.is_at_rest() {
                break;
            };
        }
        println!(
            "It took {elapsed}ms to come to a rest.",
            elapsed = elapsed.as_millis()
        );

        assert!(
            elapsed > too_short,
            "It took too short! {elapsed:?} ≤ {too_short:?} "
        );
    }

    #[test]
    fn stregth_100_dampness_3() {
        test_with(TestSetup {
            strength: 100.0,
            dampness: 3.0,
            target: 200.0,
            seconds_too_short: 1.5,
            seconds_too_long: 2.0,
            fps: 60.0,
        })
    }

    #[test]
    fn stregth_3_2_dampness_1() {
        test_with(TestSetup {
            strength: 3.2,
            dampness: 1.0,
            target: 1.0,
            seconds_too_short: 6.0,
            seconds_too_long: 7.0,
            fps: 60.0,
        })
    }
}
