use std::{fmt::Display, time::Duration};

use itertools::Itertools;
use web_sys::SvgRect;

use crate::spring::Spring;

#[derive(Clone, Copy, Debug)]
pub struct ViewBox {
    springs: [Spring; 4],
}

impl ViewBox {
    pub fn new(strength: f32, dampness: f32, value: [f32; 4]) -> Self {
        Self {
            springs: value.map(|component| {
                let mut spring = Spring::new(strength, dampness);
                spring.set_target(component);
                spring.jump_to(component);
                spring
            }),
        }
    }

    pub fn animate(&mut self, delta: &Duration) {
        for spring in self.springs.iter_mut() {
            spring.animate(delta);
        }
    }

    pub fn set_target(&mut self, target: [f32; 4]) {
        for (index, value) in target.iter().enumerate() {
            self.springs[index].set_target(*value);
        }
    }

    pub fn is_at_rest(&self) -> bool {
        self.springs.iter().all(|spring| spring.is_at_rest())
    }

    pub fn rect_components(rect: &SvgRect) -> [f32; 4] {
        [rect.x(), rect.y(), rect.width(), rect.height()]
    }
}

impl Display for ViewBox {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let string = self
            .springs
            .map(|spring| spring.get_value())
            .iter()
            .join(" ");
        f.write_str(&string)
    }
}
