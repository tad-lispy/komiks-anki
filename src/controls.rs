use crate::fathom;
use crate::svg::EmbededSvgDocument;
use gloo_events::EventListener;
use gloo_render::{request_animation_frame, AnimationFrame};
use regex::Regex;
use std::{collections::VecDeque, convert::identity, time::Duration};
use wasm_bindgen::{JsCast, UnwrapThrowExt};
use web_sys::{window, HtmlInputElement, SvgElement, SvgGraphicsElement, SvgsvgElement};
use yew::prelude::*;

pub enum Message {
    KayPressed(String),
    InputFocused,
    InputBlured,
    HintTapped,
    UserInputChanged(String),
    FormSubmitted,
    Frame(f64, Duration),
}

#[derive(Properties, Clone, Debug, PartialEq)]
pub struct Props {
    pub svg: SvgsvgElement,
    pub show_form: bool,
}

pub struct App {
    user_input: String,
    svg: EmbededSvgDocument,
    panels: VecDeque<SvgElement>,
    current_panel: Option<SvgElement>,
    blanks: Vec<SvgElement>,
    missing_word: Option<SvgElement>,
    user_guess: UserGuess,
    animation: Option<Box<AnimationFrame>>,
    input_focused: bool,
    input_element: NodeRef,
    // Keep reference so the DOM listener is not removed. See gloo_events docs.
    _keyboard_listener: EventListener,
}

#[derive(PartialEq, Eq)]
pub enum UserGuess {
    NoGuess,
    Correct { guess: String },
    Incorrect { guess: String, missing: String },
}

impl Default for UserGuess {
    fn default() -> Self {
        UserGuess::NoGuess
    }
}

impl UserGuess {
    fn new(missing: &str, guess: &str) -> Self {
        let guess = guess.to_lowercase();
        let missing = missing.to_lowercase();

        if guess == missing {
            UserGuess::Correct { guess }
        } else {
            UserGuess::Incorrect { guess, missing }
        }
    }

    fn is_done(&self) -> bool {
        match self {
            UserGuess::NoGuess => false,
            UserGuess::Correct { .. } => true,
            UserGuess::Incorrect { .. } => true,
        }
    }
}

impl App {
    pub fn new(svg: SvgsvgElement, keyboard_listener: EventListener) -> Self {
        let svg = EmbededSvgDocument::new(svg);

        // TODO: Make invalid states unrepresentable. Unify some fields into enum types.
        // Eg. user_input only has sense if there is a missing_word (i.e. it's
        // not None). Same with user_guess. If there is no blank, the guess
        // doesn't make sense. This should also simplify view logic.
        Self {
            user_input: String::default(),
            panels: svg.get_panels().into(),
            current_panel: None,
            blanks: Vec::default(),
            missing_word: None,
            user_guess: UserGuess::NoGuess,
            animation: None,
            input_focused: false,
            input_element: NodeRef::default(),
            _keyboard_listener: keyboard_listener,
            svg,
        }
    }

    fn reset(&mut self) {
        self.svg.zoom_out();
        self.svg.deselect_panels();
        // TODO: This seems error prone. Is there a way to make the compiler check if I didn't forget a field here?
        self.panels = self.svg.get_panels().into();
        self.current_panel = None;
        self.blanks = Vec::default();
        self.missing_word = None;
        self.user_guess = UserGuess::NoGuess;
    }

    fn advance(&mut self) {
        self.svg.mark_as_done(false);
        self.user_input = String::new();
        self.user_guess = UserGuess::NoGuess;
        self.missing_word = self.blanks.pop();

        if let Some(element) = &self.missing_word {
            self.svg.blank_word(&element);
            self.svg.fill_the_blank(&element, "");
            self.focus_input();
        } else {
            self.current_panel = self.panels.pop_front();

            if let Some(panel) = &self.current_panel {
                self.svg.select_panel(panel);
                let element = panel
                    .to_owned()
                    .dyn_into::<SvgGraphicsElement>()
                    .expect_throw("A panel should be a graphics element!");
                self.svg.zoom_to(&element);
                self.blanks = self.svg.select_random_words(panel, 3);
            } else {
                // No more panels. Reset.
                fathom::report_document_completion();
                self.svg.mark_as_done(true);
                self.reset();
            }
        }
    }

    fn process_answer(&mut self) {
        match &self.user_guess {
            UserGuess::NoGuess => {
                if let Some(missing) = &self.missing_word {
                    if self.user_input.is_empty() {
                        self.focus_input();
                        return;
                    }
                    self.svg.unblank_words();
                    let word = missing
                        .dataset()
                        .get("word")
                        .expect_throw("The blank word should have the 'word' data attribute!");
                    self.user_guess = UserGuess::new(&word, &self.user_input);
                    fathom::report_answer(&self.user_guess);
                    self.user_input = String::new();
                    self.svg.mark_guessed_word(missing, &self.user_guess);
                } else {
                    self.advance()
                }
            }
            UserGuess::Correct { .. } => self.advance(),
            UserGuess::Incorrect { missing, .. } => {
                if let Some(blank) = &self.missing_word {
                    self.svg.fill_the_blank(&blank, &missing);
                    self.svg.mark_as_checked(&blank);
                }
                self.advance()
            }
        }
    }

    fn focus_input(&mut self) {
        if let Some(element) = self.input_element.cast::<HtmlInputElement>() {
            element
                .focus()
                .expect_throw("It should be possible to autofocus on the input element!");
        }
    }
}

impl Component for App {
    type Message = Message;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        let props = ctx.props();

        // Send initial frame message
        ctx.link()
            .send_message(Message::Frame(0.0, Duration::new(0, 0)));

        // Setup global keyboard listener
        let body = window()
            .expect_throw("There should be a window here!")
            .document()
            .expect_throw("There should be a document in the window!")
            .body()
            .expect_throw("The document should have a body!");
        let on_key_press = ctx.link().callback(|key: String| Message::KayPressed(key));
        let keyboard_listener = EventListener::new(&body, "keypress", move |event| {
            let key = event
                .clone()
                .dyn_into::<KeyboardEvent>()
                .expect_throw("The keypress event should be a KeyboardEvent!")
                .key();
            on_key_press.emit(key)
        });

        Self::new(props.svg.to_owned(), keyboard_listener)
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Message::InputFocused => {
                self.input_focused = true;
            }
            Message::InputBlured => {
                self.input_focused = false;
            }
            Message::HintTapped => {
                if self.missing_word.is_none() || self.user_guess.is_done() {
                    // Nothing is missing or we are done. Advancing.
                    self.advance();
                } else if self.missing_word.is_some() && self.input_focused {
                    // FIXME: This never works, because as soon as the mouse or touch downs on the hint, the input is blurred.
                    // Something is missing. Input is focused. Processing answer.
                    self.process_answer();
                } else {
                    // There is no focus. Focusing.
                    self.focus_input();
                }
            }
            Message::UserInputChanged(value) => {
                if self.user_guess != UserGuess::NoGuess || self.missing_word.is_none() {
                    return true;
                }
                // TODO: Figure out if compiling the regex here is bad for performance.
                let blank_characters =
                    Regex::new(r"\s+").expect_throw("The blank characters pattern should be valid");
                self.user_input = blank_characters.replace(&value, "").to_string();
                if let Some(blank) = &self.missing_word {
                    self.svg.fill_the_blank(&blank, &self.user_input)
                }
            }
            Message::FormSubmitted => self.process_answer(),
            Message::Frame(timestamp, duration) => {
                self.svg.animate(&duration);

                let callback = ctx.link().callback(identity);
                let previous_timestamp = timestamp;
                self.animation = Some(Box::from(request_animation_frame(move |timestamp| {
                    let duration = Duration::from_millis((timestamp - previous_timestamp) as u64);
                    let msg = Message::Frame(timestamp, duration);
                    callback.emit(msg);
                })));
            }
            Message::KayPressed(key) => {
                if key == "Enter" {
                    if self.missing_word.is_none() || self.user_guess.is_done() {
                        // Nothing is missing or we are done. Advancing.
                        self.advance();
                    } else if self.missing_word.is_some() && self.input_focused {
                        // Something is missing. Input is focused. Processing answer.
                        self.process_answer();
                    } else {
                        // There is no focus. Focusing.
                        self.focus_input();
                    }
                } else {
                    ctx.link()
                        .send_message(Message::UserInputChanged(self.user_input.clone() + &key));
                    self.focus_input();
                }
            }
        };

        // TODO: Return a boolean from each branch of the match expression.
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let oninput = ctx.link().callback(|event: InputEvent| {
            event.stop_propagation();
            let value = event
                .target()
                .expect_throw("The input event should have a target!")
                .dyn_into::<HtmlInputElement>()
                .expect_throw("The input event target should be an HTML input element!")
                .value();
            Message::UserInputChanged(value)
        });

        let onclick = ctx.link().callback(|event: MouseEvent| {
            event.prevent_default();
            Message::HintTapped
        });
        let onsubmit = ctx.link().callback(|event: SubmitEvent| {
            event.prevent_default();
            Message::FormSubmitted
        });
        let onfocus = ctx.link().callback(|_| Message::InputFocused);
        let onblur = ctx.link().callback(|_| Message::InputBlured);

        let mut hint_class = classes!();
        if !self.input_focused {
            hint_class.push("highlight")
        } else {
            match &self.user_guess {
                UserGuess::NoGuess => hint_class.push(""),
                UserGuess::Correct { .. } => hint_class.push("correct"),
                UserGuess::Incorrect { .. } => hint_class.push("incorrect"),
            }
        };

        let form_class = if ctx.props().show_form {
            "visible"
        } else {
            "hidden"
        };

        let hint = if self.current_panel.is_none() {
            html! {
                <p>
                    { "Press enter or tap here to start." }
                </p>
            }
        } else if self.missing_word.is_none() {
            if self.blanks.is_empty() {
                html! {
                    <p>
                        { "Tap here or press enter to continue." }
                    </p>
                }
            } else {
                html! {
                    <p>
                        { "Read the panel."}
                        <br />
                        <small>{ "Tap here or press enter to continue." }</small>
                    </p>
                }
            }
        } else {
            match &self.user_guess {
                UserGuess::NoGuess => {
                    // TODO: If not focused, display hint for keyboard.
                    // E.g. https://fontawesome.com/icons/keyboard?f=classic&s=thin&an=beat-fade
                    hint_class.push("typing");
                    html! {
                        <p>
                            { "Type the missing word: " }
                            <span class = "user-input">
                                { self.user_input.to_lowercase() }
                            </span>
                            <br />
                            <small> { "Press enter to check." } </small>
                        </p>
                    }
                }
                UserGuess::Correct { guess } => html! {
                    <p>
                        { format!("Correct! The word is '{guess}'!") }
                        <br />
                        <small>{ "Tap here or enter to continue." }</small>
                    </p>
                },

                UserGuess::Incorrect { guess, missing } => html! {
                    <p>
                        { format!("It's not '{guess}'!") }
                        <br />
                        { format!("The word is '{missing}'!") }
                        <br />
                        <small>{ "Tap here or enter to continue." }</small>
                    </p>
                },
            }
        };

        html! {
            <>
                <form { onsubmit } class = { form_class }>
                    <input
                        type = "text"
                        name = "missing"
                        ref = { self.input_element.clone() }
                        value = { self.user_input.clone() }
                        autocomplete = { "off" }
                        id = "blank-word-input"
                        onkeypress = { |event: KeyboardEvent| { event.stop_propagation() } }
                        { oninput }
                        { onfocus }
                        { onblur }
                    />
                    <input type="submit" value="↵"/>
                </form>
                <div
                    id = "ka-hint-overlay"
                    class = "visual-fill"
                    { onclick }
                >
                    <div id = "ka-hint" class = { hint_class }>
                        { hint }
                    </div>
                </div>
            </>
        }
    }
}
