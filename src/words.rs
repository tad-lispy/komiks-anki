use unicode_segmentation::UnicodeSegmentation;


#[derive(Debug, PartialEq, Eq)]
pub enum TextFragment {
    Word(String),
    NotWord(String),
}

impl TextFragment {
    pub fn value(&self) -> String {
        match self {
            TextFragment::Word(word) => word.clone(),
            TextFragment::NotWord(value) => value.clone(),
        }
    }

    pub fn is_word(&self) -> bool {
        match self {
            TextFragment::Word(_) => true,
            TextFragment::NotWord(_) => false,
        }
    }
}

pub fn detect_words(text: &str) -> Vec<TextFragment> {
    let mut fragments: Vec<TextFragment> = Vec::new();
    let words = text.unicode_word_indices();
    let mut position = 0;

    for (index, word) in words {
        let before = index - position;
        if before != 0 {
            // There are some non-word graphemes followed by a word
            let fragment = text[position..index].to_string();
            fragments.push(TextFragment::NotWord(fragment));
            position = index;
        }
        // A new word starts here
        fragments.push(TextFragment::Word(word.to_string()));
        position += word.len();
    }

    if position != text.len() {
        // There is a leftover non-word fragment at the end of the text
        let fragment = text[position..].to_string();
        fragments.push(TextFragment::NotWord(fragment));
    }
    fragments
}

#[cfg(test)]
mod detect_words_tests {
    use super::*;

    #[test]
    fn basic_sentence_english() {
        let input = "Look! There is a lovely squirrel there!";
        let expected = vec![
            TextFragment::Word("Look".to_string()),
            TextFragment::NotWord("! ".to_string()),
            TextFragment::Word("There".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("is".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("a".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("lovely".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("squirrel".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("there".to_string()),
            TextFragment::NotWord("!".to_string()),
        ];
        let actual = detect_words(input);
        assert_eq!(expected, actual);
    }

    #[test]
    fn basic_sentence_tigrinya() {
        // TODO: Confirm with Jewiet wether this is proper Tigrinya.
        let input = "ኣብቲ ርአ፦ ሽኮሪና ምጹጽላይ ኣላ፦";

        let expected = vec![
            TextFragment::Word("ኣብቲ".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("ርአ".to_string()),
            TextFragment::NotWord("፦ ".to_string()),
            TextFragment::Word("ሽኮሪና".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("ምጹጽላይ".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("ኣላ".to_string()),
            TextFragment::NotWord("፦".to_string()),
        ];
        let actual = detect_words(input);
        assert_eq!(expected, actual);
    }

    #[test]
    fn basic_sentence_spanish() {
        let input = "¡Mira allí! ¡Hay una ardilla preciosa allí!";
        let expected = vec![
            TextFragment::NotWord("¡".to_string()),
            TextFragment::Word("Mira".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("allí".to_string()),
            TextFragment::NotWord("! ¡".to_string()),
            TextFragment::Word("Hay".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("una".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("ardilla".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("preciosa".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("allí".to_string()),
            TextFragment::NotWord("!".to_string()),
        ];
        let actual = detect_words(input);
        assert_eq!(expected, actual);
    }

    #[test]
    fn basic_sentence_polish() {
        let input = "Popatrz! Jakaż to ładna wiewiórka!";
        let expected = vec![
            TextFragment::Word("Popatrz".to_string()),
            TextFragment::NotWord("! ".to_string()),
            TextFragment::Word("Jakaż".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("to".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("ładna".to_string()),
            TextFragment::NotWord(" ".to_string()),
            TextFragment::Word("wiewiórka".to_string()),
            TextFragment::NotWord("!".to_string()),
        ];
        let actual = detect_words(input);
        assert_eq!(expected, actual);
    }

    #[test]
    fn empty_input() {
        let input = "";
        let expected: Vec<TextFragment> = vec![];
        let actual = detect_words(input);
        assert_eq!(expected, actual);
    }

    #[test]
    fn whitespace_only() {
        let input = "   \n \t";
        let expected: Vec<TextFragment> = vec![TextFragment::NotWord("   \n \t".to_string())];
        let actual = detect_words(input);
        assert_eq!(expected, actual);
    }

    #[test]
    fn single_word_english() {
        let input = "love";
        let expected: Vec<TextFragment> = vec![TextFragment::Word("love".to_string())];
        let actual = detect_words(input);
        assert_eq!(expected, actual);
    }

    #[test]
    fn single_word_polish() {
        let input = "miłość";
        let expected: Vec<TextFragment> = vec![TextFragment::Word("miłość".to_string())];
        let actual = detect_words(input);
        assert_eq!(expected, actual);
    }

    #[test]
    fn single_word_tigrinya() {
        let input = "ፍቅሪ";
        let expected: Vec<TextFragment> = vec![TextFragment::Word("ፍቅሪ".to_string())];
        let actual = detect_words(input);
        assert_eq!(expected, actual);
    }
}
