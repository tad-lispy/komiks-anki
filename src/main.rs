mod controls;
mod fathom;
mod spring;
mod svg;
mod viewbox;
mod words;

use gloo_console as console;
use gloo_net::http::Request;
use js_sys::Array;
use std::panic;
use std::{collections::HashMap, ops::Add};
use wasm_bindgen::{prelude::wasm_bindgen, JsCast, JsValue, UnwrapThrowExt};
use wasm_bindgen::{throw_val, JsError};
use wasm_bindgen_futures::{spawn_local, JsFuture};
use web_sys::{
    window, DomParser, File, HtmlElement, HtmlFormElement, HtmlInputElement, SubmitEvent,
    SupportedType, SvgElement, SvgsvgElement, Url, UrlSearchParams,
};

#[wasm_bindgen(module = "/src/visual-viewport-hack.js")]
extern "C" {
    fn setup_visual_viewport_hack();
}

trait WithRelativePath {
    fn relative_path(&self) -> String;
}

impl WithRelativePath for File {
    fn relative_path(&self) -> String {
        js_sys::Reflect::get(self, &JsValue::from("webkitRelativePath"))
            .expect_throw("A file JS object should have the webkitRelativePath property!")
            .as_string()
            .expect_throw("The value of the webkitRelativePath property should be a string!")
    }
}

type Blobs = HashMap<String, File>;

/** This is the onsubmit handler for the upload form in the index.html */
#[wasm_bindgen]
pub async fn process_upload(event: SubmitEvent) {
    console::log!("Processing upload (Rust)");
    event.prevent_default();

    const BASE_URL: &str = "file:///uploaded/";

    let form = event
        .target()
        .expect_throw("The submit event should have a target!")
        .dyn_into::<HtmlFormElement>()
        .expect_throw("The target of the submit event should be an HTML form!");
    let input = form
        .query_selector("input[type='file']")
        .expect_throw("The query for a file input should be valid!")
        .expect_throw("There should be a file input in the upload form!")
        .dyn_into::<HtmlInputElement>()
        .expect_throw("The file input element should be an HTML input element!");

    // TODO: Remove once not required
    let document = form
        .owner_document()
        .expect_throw("The form should belong to a document");

    let files = input
        .files()
        .expect_throw("There should be files in the file input!");
    let mut blobs: Blobs = HashMap::new();
    for file in Array::from(&files).iter() {
        let file = file
            .dyn_into::<File>()
            .expect_throw("An item in the files list should be a file!");
        let relative_path = file.relative_path();
        let file_url = Url::new_with_base(&relative_path, BASE_URL)
            .expect_throw("It should be possible to create a file URL from an uploaded file!");

        blobs.insert(file_url.href(), file);
    }

    // Select SVG file
    // TODO: What if there is more than one SVG file in the upload?
    let (file_url, svg_file) = blobs
        .iter()
        .find(|(_url, file)| file.type_() == "image/svg+xml")
        .expect_throw("There should be an SVG file in the upload!");

    let file_url = Url::new_with_base(&file_url, BASE_URL)
        .expect_throw("It should be possible to create a file URL for the uploaded SVG file!");
    console::log!("Using", file_url.href());
    let svg_code = JsFuture::from(svg_file.text())
        .await
        .expect_throw("It should be possible to read text from the SVG file!")
        .as_string()
        .expect_throw("The text content of the SVG file should be a string!");

    let svg = parse_svg(svg_code, false, file_url, &Some(blobs));
    attach_content(&document, &svg);
    setup_controls(&document, svg)
}

fn main() {
    spawn_local(async move { initalize().await });
}

async fn initalize() {
    let debug: bool = get_query_param("debug").is_some();

    let window = window().expect("There should be a window here!");

    let location_href = window
        .location()
        .href()
        .expect_throw("There should be an href in the window location!");
    let location_url = Url::new(&location_href)
        .expect_throw("It should be possible to construct a URL from location href!");

    match get_redirect_url(&location_url) {
        Ok(None) => {
            // It's all good!
        }
        Ok(Some(redirection)) => {
            window
                .location()
                .replace(&redirection.href())
                .expect_throw("It should be possible to replace the window location!");
            return;
        }
        Err(error) => {
            throw_val(error);
        }
    }

    let document = window
        .document()
        .expect_throw("There should be a document in the window!");

    // Load remote content
    let Some(content_url) = get_content_url(&location_url) else {
        console::log!("No content URL provided.");
        mark_as_loaded(&document);
        return
    };

    let code = get_content(&content_url).await;
    let svg = parse_svg(code, debug, content_url, &None);
    attach_content(&document, &svg);

    // Start the UI app
    setup_controls(&document, svg);
    mark_as_loaded(&document);
}

fn setup_controls(document: &web_sys::Document, svg: SvgsvgElement) {
    let container = document
        .query_selector("#ka-controls")
        .expect_throw("Query for the controls div should be valid!")
        .expect_throw("There should be an element with id 'ka-controls' where the application UI will be rendered!" );
    let show_form = false;
    // TODO: Use query parameters to control this
    yew::Renderer::<controls::App>::with_root_and_props(
        container,
        controls::Props { svg, show_form },
    )
    .render();

    setup_visual_viewport_hack();
}

fn parse_svg(code: String, debug: bool, content_url: Url, blobs: &Option<Blobs>) -> SvgsvgElement {
    let svg = parse_content(code);

    svg.class_list()
        .add_1("ka-svg-document")
        .expect_throw("It should be possible to add a class to the SVG document!");
    if debug {
        svg.dataset()
            .set("debug", "true")
            .expect_throw("It should be possible to set data-debug attribute on the SVG document.");
    }

    substitute_hrefs(&svg);

    // Resolve URLs in links (e.g. stylesheets), images, etc.
    rewrite_links(&svg, &content_url.href(), blobs);
    rewrite_images(&svg, &content_url.href(), blobs);
    svg
}

/*** Find all elements with data-href attribute, and use it's value for xlink:href
 *
 * This is a hack to use webp, avif or other image formats that are not yet supported by Inkscape.
 *
 * TODO: Consider removing this code once https://gitlab.com/inkscape/inbox/-/issues/1952 is addressed
 *
 * */
fn substitute_hrefs(svg: &SvgsvgElement) {
    let elements = svg
        .query_selector_all("[data-href]")
        .expect_throw("The query for elements with a data-href attribute should be valid!");

    for element in Array::from(&elements).iter() {
        if let Err(error) = substitute_href(&element) {
            console::error!(&error);
            console::error!("Failed to substitute href for this element", &element);
            continue;
        };
    }
}

fn substitute_href(element: &JsValue) -> Result<(), JsValue> {
    let element = element.to_owned().dyn_into::<SvgElement>()?;
    let Some(href) = element.dataset().get("href") else {
        let error = JsError::new(
            "The element with the data-href attribute should have an href key in it's data set (obviously)!"
        );
        return Result::Err(error.into());
    };
    let namespace = element.lookup_namespace_uri(Some("xlink"));
    element.set_attribute_ns(namespace.as_deref(), "href", &href)?;
    Ok(())
}

fn mark_as_loaded(document: &web_sys::Document) {
    document
        .document_element()
        .expect_throw("The HTML document should have a root element!")
        .class_list()
        .add_1("ka-ready")
        .expect_throw(
            "It should be possible to add a class to the root element of the HTML document!",
        );
}

/// Resolve href attribute in all link elements to absolute URLs,
///
/// If blobs mapping is given and the absolute URL is found there, then use the
/// [object URL][] to a blob. This is useful when local content is uploaded
/// during development. See the [process_upload] function.
///
/// [object URL]: https://developer.mozilla.org/en-US/docs/Web/API/URL/createObjectURL
///
fn rewrite_links(svg: &SvgsvgElement, base_href: &str, blobs: &Option<Blobs>) {
    let links = svg
        .query_selector_all("link")
        .expect_throw("The query for link elements in the SVG document should be valid!");

    for link in Array::from(&links).iter() {
        let link = link
            .dyn_into::<HtmlElement>()
            .expect_throw("A link element in the SVG content should be an HTML element.");
        let Some(href) = link.get_attribute("href") else {
            console::warn!("A link without an href attribute? Peculiar.", link);
            continue
        };
        let resolved_url = Url::new_with_base(&href, base_href).expect_throw(
            "It should be possible to resolve a URL from the link and the content URL!",
        );

        let href = blobs
            .as_ref()
            .and_then(|blobs| blobs.get(&resolved_url.href()))
            .map(|blob| {
                Url::create_object_url_with_blob(blob)
                    .expect_throw("It should be possible to construct an object URL from a blob!")
            })
            .unwrap_or(resolved_url.href());

        // link.set_attribute(namespace.as_deref(), "href", resolved_url.href())
        link.set_attribute("href", &href)
            .expect_throw("It should be possible to set href attribute of a link element!");

        console::log!("Resolved href for a link in the SVG document", &link);
    }
}

fn rewrite_images(svg: &SvgsvgElement, base_href: &str, blobs: &Option<Blobs>) {
    let images = svg
        .query_selector_all("image")
        .expect_throw("The query for image elements in the SVG document should be valid!");

    for image in Array::from(&images).iter() {
        let image = image
            .dyn_into::<SvgElement>()
            .expect_throw("An image element in the SVG content should be an SVG element.");

        let namespace = image.lookup_namespace_uri(Some("xlink"));

        let Some(href) = image.get_attribute_ns(namespace.as_deref(), "href") else {
            console::warn!("An image without an href attribute? Peculiar.", image);
            continue
        };
        let resolved_url = Url::new_with_base(&href, base_href).expect_throw(
            "It should be possible to resolve a URL from the image and the content URL!",
        );

        // Check the blobs mapping and substitute with object URL
        let href = blobs
            .as_ref()
            .and_then(|blobs| blobs.get(&resolved_url.href()))
            .map(|blob| {
                Url::create_object_url_with_blob(blob)
                    .expect_throw("It should be possible to construct an object URL from a blob!")
            })
            .unwrap_or(resolved_url.href());

        // link.set_attribute(namespace.as_deref(), "href", resolved_url.href())
        image
            .set_attribute("href", &href)
            .expect_throw("It should be possible to set href attribute of an image element!");

        console::log!("Resolved href for an image in the SVG document", &image);
    }
}

fn attach_content(document: &web_sys::Document, svg: &SvgsvgElement) {
    let container = document
        .query_selector("#ka-content")
        .expect_throw("There query for the container element should be valud!")
        .expect_throw("There should be a container element to place the content in!");

    // Is this very bad? Clearing the content with web_sys seems hard.
    container.set_inner_html("");

    container
        .append_child(svg)
        .expect_throw("It should be possible to append the SVG document to the container!");
}

fn parse_content(code: String) -> SvgsvgElement {
    let parser = DomParser::new().expect_throw("It should be possible to create a DOM parser!");

    let svg = parser
        .parse_from_string(&code, SupportedType::ImageSvgXml)
        .expect_throw("It should be possible to parse ")
        .root_element()
        .expect_throw("There should be a root element in the SVG document!");
    svg
}

async fn get_content(content_url: &Url) -> String {
    let response = Request::get(&content_url.href()).send().await.unwrap();
    let code = response
        .text()
        .await
        .expect("The response should contain text!");
    code
}

fn get_redirect_url(url: &Url) -> Result<Option<Url>, JsValue> {
    let location_href = url.href();

    let Some(value) = get_query_param("content") else {
        return Ok(None);
    };
    let Some(path) = replace_suffix(&value, ".svg", ".html") else {
        let error = JsError::new("The value of the content query parameter should end with .svg suffix!");
        return Err(error.into());
    };
    Url::new_with_base(&path, &location_href).map(Some)
}

fn get_content_url(location_url: &Url) -> Option<Url> {
    let location_href = location_url.href();
    let location_path = location_url.pathname();

    if location_path.starts_with("/content/") {
        let content_path = replace_suffix(&location_path, ".html", ".svg").expect_throw(
            "The location path that starts with /content/ should end with the .html suffix!",
        );
        let content_url = Url::new_with_base(&content_path, &location_href)
            .expect_throw("It should be possible to construct new URL from the modified path!");
        Some(content_url)
    } else {
        None
    }
}

fn replace_suffix(subject: &str, suffix: &str, replacement: &str) -> Option<String> {
    if let Some(stripped) = subject.to_string().strip_suffix(suffix) {
        Some(stripped.to_string().add(replacement))
    } else {
        None
    }
}

fn get_query_param(key: &str) -> Option<String> {
    let window = window().expect_throw("There should be a window here!");
    let search = window
        .location()
        .search()
        .expect_throw("The window should have a location with the search part!");
    let params = UrlSearchParams::new_with_str(&search).expect_throw(
        "It should be possible to parse search part of the location into UrlSearchParams object!",
    );

    params.get(key)
}
