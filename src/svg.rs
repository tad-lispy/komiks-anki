use crate::{controls::UserGuess, viewbox::ViewBox, words};
use gloo_console as console;
use js_sys::Array;
use rand::seq::IteratorRandom;
use std::{ops::Not, time::Duration};
use wasm_bindgen::{prelude::*, throw_str};
use web_sys::{window, Node, SvgElement, SvgGraphicsElement, SvgsvgElement};

pub struct EmbededSvgDocument {
    svg: web_sys::SvgsvgElement,
    rng: rand::rngs::ThreadRng,
    viewbox: ViewBox,
}

impl EmbededSvgDocument {
    pub fn new(svg: SvgsvgElement) -> Self {
        let svg = svg
            .to_owned()
            .dyn_into::<SvgsvgElement>()
            .expect_throw("The embedded element should be an SVG document");

        svg.set_attribute("preserveAspectRatio", "xMidYMin meet")
            .expect_throw(
                "It should be possible to set preserveAspectRatio attribute on the SVG element",
            );

        let rect = svg
            .view_box()
            .base_val()
            .expect_throw("The SVG document should have a view box with a base value!");
        let viewbox = ViewBox::new(100.0, 5.0, ViewBox::rect_components(&rect));

        let document = Self {
            svg,
            viewbox,
            rng: rand::thread_rng(),
        };

        document.mark_panels();
        document.mark_word_spans();

        document
    }

    pub fn mark_panels(&self) {
        let elements = self
            .svg
            .query_selector_all("svg > g:not([data-ka-ignore]) > g:not([data-ka-ignore])")
            .expect_throw("The panels query should be valid!");
        for element in Array::from(&elements).iter() {
            element
                .dyn_into::<SvgElement>()
                .expect_throw("A panel should be an SVG Element")
                .class_list()
                .add_1("ka-panel")
                .expect_throw(
                    "It should be possible to add a class to a panel (an SVG g element)!",
                );
        }
    }

    pub fn get_panels(&self) -> Vec<SvgElement> {
        let elements = self
            .svg
            .query_selector_all(".ka-panel")
            .expect_throw("The panels query should be valid!");

        Array::from(&elements)
            .iter()
            .map(move |element| -> SvgElement {
                element
                    .dyn_into::<SvgElement>()
                    .expect_throw("A panel should be an SVG Element")
            })
            .collect()
    }

    pub fn fill_the_blank(&self, blank: &SvgElement, content: &str) {
        if content.trim().is_empty() {
            blank.set_text_content(Some("···"));
            blank
                .class_list()
                .add_1("empty")
                .expect_throw("It should be possible to add the empty class to the blank element!");
        } else {
            blank.set_text_content(Some(&content.to_lowercase()));
            blank.class_list().remove_1("empty").expect_throw(
                "It should be possible to remove the empty class from the blank element!",
            );
        };
    }

    pub fn zoom_out(&mut self) {
        let container = self
            .svg
            .query_selector("svg > g")
            .expect_throw("A query for top-level group should be valid!")
            .expect_throw("There should be a top level group (a layer) in the SVG document!")
            .dyn_into::<SvgGraphicsElement>()
            .expect_throw("The SVG should be a graphics element!");
        self.zoom_to(&container);
    }

    pub fn zoom_to(&mut self, target: &SvgGraphicsElement) {
        if target
            .nearest_viewport_element()
            .expect_throw("There should be a viewport element around the element!")
            .eq(&self.svg)
            .not()
        {
            console::error!("The zoom target", target);
            console::error!("The embedded SVG document", &self.svg);
            throw_str("The zoom target is not an element of the this SVG document!");
        }

        let rect = target
            .get_b_box()
            .expect_throw("The zoom target should have a bounding box!");
        self.viewbox.set_target(ViewBox::rect_components(&rect));
    }

    pub fn animate(&mut self, delta: &Duration) {
        if self.viewbox.is_at_rest() {
            return;
        };

        self.viewbox.animate(delta);
        self.svg
            .set_attribute("viewBox", &self.viewbox.to_string())
            .expect_throw("The viewbox should have been updated!");
    }

    pub fn unblank_words(&self) {
        let spans = self
            .svg
            .query_selector_all("tspan.word")
            .expect_throw("The words query should be valid!");
        for span in Array::from(&spans).iter() {
            span.dyn_into::<SvgElement>()
                .expect_throw("The tspan.word should be an SVG element!")
                .class_list()
                .remove_1("blank")
                .unwrap();
        }
    }

    pub fn mark_as_checked(&self, word: &SvgElement) {
        let classes = word.class_list();
        classes.remove_1("blank").unwrap();
        classes.add_1("checked").unwrap();

        // Restore kerning on the parent element. See the blank_word function, where the kerning is disabled.
        let Some(parent) = word.parent_element() else { return };
        let Some(dx) = parent.get_attribute("data-dx") else {
            return
        };
        parent.set_attribute("dx", &dx).expect_throw(
            "It should be possible to copy a value of the data-dx attribute to the dx attribute of a word's parent element.",
        );
        parent.remove_attribute("data-dx").expect_throw(
            "It should be possible to remove the data-dx attribute from a parent of a word.",
        );
    }

    pub fn mark_word_spans(&self) {
        let namespace = self
            .svg
            .namespace_uri()
            .expect_throw("The embedded SVG document should have a namespace!");

        let spans = self
            .svg
            .query_selector_all("tspan:not([data-ka-ignore] *)")
            .expect_throw("The query for tspan elements should have worked!");

        let document = self
            .svg
            .owner_document()
            .expect_throw("The SVG element should be embedded in a document!");

        for span in Array::from(&spans).iter() {
            let span = span
                .dyn_into::<SvgElement>()
                .expect_throw("The element containing a word should be an SVG element!");
            let style = window()
                .expect_throw("There should be a window here!")
                .get_computed_style(&span)
                .expect_throw("It should be possible to get a computed style of the tspan element!")
                .expect_throw("The span should have a computed style!");
            let color = style.get_property_value("fill").unwrap();
            if color == "rgb(128, 0, 0)" {
                console::log!("Skipping marked with maroon color.", span.clone());
                span.style().remove_property("fill").unwrap();
                continue;
            }

            for node in Array::from(&span.child_nodes()).iter() {
                let node = node
                    .dyn_into::<Node>()
                    .expect_throw("A child node of a tspan element should be a node, duh!");
                let node_type = node.node_type();
                if node_type == Node::TEXT_NODE {
                    let text = node
                        .text_content()
                        .expect_throw("The text node should contain text!");
                    for fragment in words::detect_words(&text).iter() {
                        let word_span = document
                            .create_element_ns(Some(namespace.as_str()), "tspan")
                            .expect_throw(
                                "It should be possible to create a tspan element for a word!",
                            )
                            .dyn_into::<SvgElement>()
                            .expect_throw("The newly created tspan should be an SVG element!");

                        word_span.set_text_content(Some(&fragment.value()));
                        if fragment.is_word() {
                            word_span.dataset()
                            .set("word", &fragment.value().to_uppercase())
                            .expect_throw("It should be possible to set the 'word' data attribute on the element!");
                            word_span.class_list().set_value("word");
                        }
                        span.insert_before(&word_span, Some(&node)).expect_throw(
                            "It should be possible to insert the word span before text node!",
                        );
                    }
                    span.remove_child(&node).unwrap();
                }
            }
        }
    }

    pub fn select_random_words(&mut self, panel: &SvgElement, amount: usize) -> Vec<SvgElement> {
        let spans = panel
            .query_selector_all("tspan.word")
            .expect_throw("The query for word elements should have been valid!");
        Array::from(&spans)
            .iter()
            .choose_multiple(&mut self.rng, amount)
            .iter()
            .map(|element| {
                element
                    .to_owned()
                    .dyn_into::<SvgElement>()
                    .expect_throw("Each of the selected word elements should be an SVG element!")
            })
            .collect()
    }

    pub fn blank_word(&self, word: &SvgElement) {
        word.class_list()
            .remove_2("correct", "incorrect")
            .expect_throw("It should be possible to remove classes from the blank word element");
        word.class_list()
            .add_1("blank")
            .expect_throw("It should be possible to set a class of the element to be blanked!");

        // Disable kerning on the parent element. Otherwise possition of letters get messed up when characters are added or removed.
        let Some(parent) = word.parent_element() else { return };
        let Some(dx) = parent.get_attribute("dx") else { return  };
        parent.set_attribute("data-dx", &dx).expect_throw(
            "It should be possible to copy the value of dx attribute to the data-dx attribute of a word's parent element.",
        );
        parent.remove_attribute("dx").expect_throw(
            "It should be possible to remove the dx attribute from a parent of a word.",
        );
    }

    pub fn select_panel(&self, panel: &SvgElement) {
        self.deselect_panels();

        panel
            .class_list()
            .add_1("ka-current-panel")
            .expect_throw("It should be possible to add a class to the current panel");
    }

    pub fn deselect_panels(&self) {
        let panels = self
            .svg
            .query_selector_all(".ka-current-panel")
            .expect_throw("The current panels query should be valid!");

        for panel in Array::from(&panels).iter() {
            panel
                .dyn_into::<SvgElement>()
                .expect_throw("The current panel should be an SVG element!")
                .class_list()
                .remove_1("ka-current-panel")
                .expect_throw("It should be possible to remove a class from the panel that is no longer current!");
        }
    }

    pub fn mark_guessed_word(&self, word: &SvgElement, user_guess: &UserGuess) {
        word.class_list()
            .remove_2("correct", "incorrect")
            .expect_throw("It should be possible to remove classes from the blank word element");

        let class = match user_guess {
            UserGuess::NoGuess => "",
            UserGuess::Correct { .. } => "correct",
            UserGuess::Incorrect { .. } => "incorrect",
        };
        word.class_list()
            .add_1(class)
            .expect_throw("It should be possible to mark the blank word element as correct!");
    }

    pub fn mark_as_done(&self, done: bool) {
        if done {
            self.svg.class_list().add_1("ka-done").expect_throw(
                "It should be possible to add the 'ka-done' class to the SVG document",
            );
        } else {
            self.svg.class_list().remove_1("ka-done").expect_throw(
                "It should be possible to remove the 'ka-done' class to the SVG document",
            );
        }
    }
}
