# Mazanki

Vul de lege ruimte in uw taal in!

van Tad, Fana, and Jij!

---

Hallo, ik ben Tad!
Ik leer Nederlands.

En ik ben Fana. Ik ben ook aan het leren.

---

Fana en ik hebben een klein hulpmiddel gemaakt om ons te helpen leren.

Het heet Mazanki.

We willen het met je delen!

---

Zo werkt het.

---

  1. De leerling ziet een afbeelding met wat tekst erop.
  2. Er wordt dan één woord verwijderd.
  3. De leerling typt het ontbrekende woord. Was het juist?
  4. Een ander woord is verwijderd...
  
---

> Uit het scherm

Wil je het proberen?

https://mazanki.app/

---

Bedankt voor je tijd!

Wat denk je?

Wil je helpen?



