# Mazanki

Fill in the blanks in your language!

by Tad, Fana, and You!

---

Hello, I'm Tad!
I'm learning Dutch.

And I'm Fana. I'm also learning.

---

Fana and I made a little tool to help us learn.

It's called Mazanki.

We want to share it with you!

---

Here is how it works.

---

  1. The student sees a picture with some text on it.
  2. One word is then removed.
  3. The student types the missing word. Was it correct?
  4. Another word is removed...
  
---

> Off screen

Would you like to try?

https://mazanki.app/

---

What do you think?

---

Thank you!

Would you like to help?
