A tool to learn languages by filling missing words from documents, like comic books, diagrams, posters, maps etc.

Currently Mazanki is in early development and we focus on our own needs within Bussum Taalcafe.

# Content Creation

Mazanki is a tool for learning languages using images and text. The content is created by people like you. Here are few ways you can help.

- Art (drawing, paintings, photos, digital art)
- Translation of existing content into other languages
- Promotion (tell your friends, share on social media)
- 

The Mazanki application is [free software](https://en.wikipedia.org/wiki/Free_software). Anyone can use or modify it. Currently all content is provided under the terms of [Creative Commons Attribution - NonCommercial 4.0 International (CC BY-NC 4.0)]() license. In the future we may introduce paid content and some ways for creators to earn money. For now it's too early to say how it will work exactly. Most likely the majority of content will always be free.


## Self Hosting Fonts

Considerations when using custom fonts:

  - Does it look good?
  - Character range (what languages does it support?)
  - Download size
  - Privacy of students


A common way of using custom fonts is via services like Google Fonts. This is very easy and might be a first step in adding custom fonts to your documents. However using such a service has a downside of leaking information about students and their activity to their operator (like Google). It is very important to respect the privacy of our students, and one way to do it is to self-host all the resources, including fonts. Here is how to do that.
 
 - Find the font you like on Google Fonts

 - Use [google-webfonts-helper](https://gwfh.mranftl.com/fonts) to download the font

 - Copy the CSS code into a file in the same directory as your document,

 - Install the font in your operating system, so that Inkscape can use it,
 
 - Link the CSS file into the SVG document
 

> TODO: More detailed, step by step instructions (maybe a video?)


# Adapting Existing Content

The main challenge is copyright. Try to find content that is licensed under one of the [Creative Commons](https://creativecommons.org/) licenses or is in [the public domain](https://en.wikipedia.org/wiki/Public_domain). If using such a content, please attribute the authors, even if it's not strictly required by the license.


## Potential sources of suitable content

  1. [Sandra and Woo](http://www.sandraandwoo.com/archive/),

     A webcomic with a CC licencse in English and German, with transcripts.
       
  2. [Pepper & Carrot](https://www.peppercarrot.com/en/)
  
     A free(libre) and open-source webcomic supported directly by its patrons to change the comic book industry! Multiple translations and sources available as SVG.
     
  3. [OpenClipArt](https://openclipart.org/)
  
     There is a lot of low-quality content there, but also some very good illustrations. The biggest challenge is to find the good ones among the bad. Users have their collections, and one way is to identify users with good quality content. 
  
     - https://openclipart.org/artist/liftarn


# Credits

The favicon was generated using the following font:

- Font Title: Caveat
- Font Author: Copyright 2014 The Caveat Project Authors (https://github.com/googlefonts/caveat)
- Font Source: http://fonts.gstatic.com/s/caveat/v17/WnznHAc5bAfYB2QRah7pcpNvOx-pjRV6SIKjYBxPigs.ttf
- Font License: SIL Open Font License, 1.1 (http://scripts.sil.org/OFL))
