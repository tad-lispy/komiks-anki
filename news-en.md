# Wednesday, 2023-06-14

## "Het lichaam" is improved

Now there is a picture of a man's face with some labels (teeth, tongue, mustache, beard, nostrils, pupils and iris). Have a look at <https://mazanki.app/content/the-human-body/the-human-body-nl.html>.


---


# Wednesday, 2023-06-07

This is the second newsletter for volunteers helping with the Mazanki project.


## "Het huis" is labeled and published

Last Friday at Taalcafe Ahmet, David, Engin and ... labeled the House diagram. It is now published at ...


## Next topic: "Beroepen"

We think about bringing a sheet with icons representing different professions and asking a small group of students to name them and write a short sentence describing each.


## Two ideas to discuss

1. An extension to web browser that would blank words on any website

   This could address the difficulty in preparing content. Students could practice vocabulary on any website they find interesting.
   
   
2. Practice listening and writing

   The program could read a sentence and the student's task would be to write it. 


Thank you for all your help.


---


# Wednesday, 2023-05-31

Dear volunteers!

With this newsletter, we want to keep you informed about what's going on with the Mazanki project. This is the first news update.


## First voting results

Four weeks ago we did a little voting exercise. We asked students and volunteers at the Bussum Taalcafe, which new topics they would like to see the most. Here are the top results (in Dutch):

  - Beroepen
  - Bezoek aan de dokter, tandarts en apotheek
  - Familie
  - Feestdagen
  - Huis
  - In de winkel (bakker, drogisterij, groentewinkel, kruidenierswinkel, parfumerie)
  - Studeren, opleiding en werk in Nederland

We would like to do the voting again, because there were not many students that day.


## "Het lichaam" is published

https://mazanki.app/content/the-human-body/the-human-body-nl.html

Thanks to the efforts of Rob, Maaike, Fadima and Fadima Merve who labeled all the different body and face parts. The on-line version is simplified for now, but we will add all the labels soon. To finish we need a good picture of male face with beard and wide grin (so the teeth are visible) and a dog with a collar.


## Next subject: Het huis

We would like a few students and a volunteer to help us by labeling different parts of the house. Last time it was a lot of fun!


## Help

The Mazanki project should belong to all students and teachers. To make it so, help is needed in creating the content:

- Drawing, painting and photos,
- Topics and ideas,
- Labeling.

Major difficulty is getting good quality images that have suitable license, like the [Creative Commons][] license.


[Creative Commons]: https://creativecommons.org/
