{
  description = "A tool to learn languages by filling missing words from comic books.";

  inputs = {
    nixpkgs.url      = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    flake-utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, rust-overlay, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        project-name = "mazanki";
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        buildInputs = with pkgs; [
          openssl
        ];
        nativeBuildInputs = with pkgs; [
          (rust-bin.fromRustupToolchainFile ./rust-toolchain.toml)
          gcc
          pkg-config
          gnumake
          findutils
          gzip
          lld
          trunk
          wasm-bindgen-cli
          brotli
          rustPlatform.bindgenHook
        ];
      in
      {
        devShell = pkgs.mkShell {
          inherit buildInputs nativeBuildInputs;
          name = "${project-name}-develpoment-shell";
          packages = with pkgs; [
            pkgs.rust-analyzer
            pkgs.miniserve
            pkgs.jq
            ocrmypdf
            tesseract
            vips
          ];
          project_name = project-name; # Expose as an environment variable for make
        };
        defaultPackage = pkgs.rustPlatform.buildRustPackage {
          name = project-name;
          version = "1.0.0";
          src = ./.;
          cargoLock = {
            lockFile = ./Cargo.lock;
          };

          inherit buildInputs nativeBuildInputs;
        };
        packages.docker-image = pkgs.dockerTools.buildLayeredImage {
          name = "${project-name}-build-image";
          tag = "latest";
          created = "now";
          contents = buildInputs ++ nativeBuildInputs ++ [
            pkgs.bash
            pkgs.coreutils
            pkgs.git
            pkgs.httpie
            pkgs.rustup
            pkgs.cacert
          ];
          fakeRootCommands = ''
            mkdir --parents /tmp
          '';
          enableFakechroot = true;
          config.Cmd = [ "${ pkgs.bash }/bin/bash" ];
        };
      }
    );
}
