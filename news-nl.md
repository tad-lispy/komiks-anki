# Woensdag, 2023-06-14


## "Het lichaam" is verbeterd

Er is een plaatje toegevoegd van het gezicht  van een man met de namen (tanden, tong, snor, baard, neusgaten, ooglid, pupil en iris). Het staat hier: [https://mazanki.app/content/the-human-body/the-human-body-nl.html](https://mazanki.app/content/the-human-body/the-human-body-nl.html).


---


# Woensdag, 2023-06-07

Dit is de tweede nieuwsbrief voor de vrijwilligers die helpen met het Mazanki project.


## "Het huis" is klaar

Vorige week vrijdag hebben Ahmet, David, Engin en ... in het Taalcafé de onderdelen van het huis de naam gegeven. Het onderwerp is gepubliceerd over <...>.


## Volgende onderwerp: "Beroepen"

Komende vrijdag proberen wij plaatjes te vinden waarop beroepen zijn afgebeeld en vragen een paar studenten de naam van het beroep te geven met een korte beschrijving.


## Twee ideeën voor discussie

1.  Een programma dat de woorden in de webpagina laat verdwijnen

    Leerlingen kunnen oefenen met alle teksten die ze interessant vinden.


2.  Luisteren en schrijven oefenen

    Het programma zal een zin uitspreken en de student zou deze correct moeten typen.


Dankjewel voor alle hulp!


---


# Woensdag, 2023-05-31

Beste vrijwilliger!

Met deze nieuwsbrief willen wij jou graag op de hoogte houden van het Mazanki project. Dit is de eerste update. We willen Barbara en Henk bedanken voor hun hulp met vertalen.


## Eerste onderzoeksresultaten

Paar weken geleden deden wij een klein onderzoekje onder de studenten. De vraag was: welke onderwerpen wil je zien in de Mazanki app? De onderwerpen die het meest werden genoemd zijn:

  - Beroepen
  - Bezoek aan de dokter, tandarts en apotheek
  - Familie
  - Feestdagen
  - Huis
  - In de winkel (bakker, drogisterij, groentewinkel, kruidenierswinkel, parfumerie)
  - Studeren, opleiding en werk in Nederland

Wij willen dit onderzoekje nog een keer doen, omdat er op die dag niet zo veel studenten aanwezig waren.


## "Het lichaam" is gepubliceerd

https://mazanki.app/content/the-human-body/the-human-body-nl.html

Rob, Maaike, Fadima en Fadima Merve hebben alle verschillende lichaamsdelen benoemd. De on-line versie is nu nog simpel, maar wij zullen alle namen er later bij zetten. Daar hebben wij  goede afbeeldingen voor nodig. De afbeeldingen die wij zoeken zijn:

  - van een gezicht van een man met baard en glimlach, (zodat je tanden kunt zien,) 

  - van een hond met halsband.


## Volgende onderwerp: Het huis

Kunnen een vrijwilliger en een paar studenten ons helpen verschillende delen van het huis te benoemen? Vorige keer was het erg leuk!

## Wil je meehelpen?

Het Mazanki project wil graag samenwerken met alle studenten en leraren. We willen dat het project eigendom is van alle studenten en leraren. Deze hulp is nodig voor de inhoud.

Wij hebben hulp nodig met:

  - Tekeningen, plaatjes, foto's;
  - Onderwerpen en ideeën;
  - Benoemen en schrijven.

Het is heel moeilijk om kwalitatief goede afbeeldingen te vinden. De afbeelding moet een juiste licentie hebben (zoals [Creative Commons][]). 


[Creative Commons]: https://creativecommons.org/


# Wednesday, 2023-06-07

This is the second newsletter for volunteers helping with the Mazanki project.


## "Het huis" is labeled and published

Last Friday at Taalcafe Ahmet, David, Engin and ... labeled the House diagram. It is now published at ...


## Next topic: "Beroepen"

We think about bringing a sheet with icons representing different professions and asking a small group of students to name them and write a short sentence describing each.


## "Het lichaam" is improved

Now there is a picture of a man's face with some labels (teeth, tongue, mustache, beard, nostrils, pupils and iris). Have a look at <https://mazanki.app/content/the-human-body/the-human-body-nl.html>.


## Two ideas to discuss

1. An extension to web browser that would blank words on any website

   This could address the difficulty in preparing content. Students could practice vocabulary on any website they find interesting.
 
 
2. Practice listening and writing

   The program could read a sentence and the student's task would be to write it. 


Thank you for all your help.
