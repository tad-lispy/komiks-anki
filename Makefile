include Makefile.d/defaults.mk

all: ## Build the program (DEFAULT)
all: test build
.PHONY: all

build: ## Build the program for distribution
build: web
.PHONY: build

test:
	cargo test
.PHONY: test

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: build
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive $</* $(prefix)/
.PHONY: install


### DEVELOPMENT

develop: ## Rebuild and run a development version of the program
develop: web/develop
.PHONY: develop

clean: ## Remove all build artifacts
clean:
	git clean -dfX \
		--exclude='!.envrc.private'
.PHONY: clean

help: ## Print this help message
help:
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help

### WEB TARGET

web: ## Build the program for the web
web: $(shell find src/)
web: $(shell find assets/)
web: Cargo.lock
web: index.html
web: base_url ?= "/"
web:
	trunk build \
		--release \
		--public-url $(base_url) \
		--dist $@
	touch $@

web/develop: ## Rebuild and serve a development version of the in a local server
web/develop:
	trunk serve \
		--address=0.0.0.0 \
		index.html
.PHONY: web/develop

web/serve: ## Serve a release version of the program
web/serve: web
web/serve:
	miniserve --index=index.html $<
.PHONY: web/serve


### CONTINUOUS INTEGRATION

public: ## Prepare the web assets (used in CI)
public: web
public:
	rm --recursive --force $@
	cp --recursive --force $^ $@
	unset GZIP
	find $@ -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\|wasm\)$$' -exec gzip -9 -f -k {} \;
	find $@ -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\|wasm\)$$' -exec brotli --force --keep {} \;
	touch $@
.PHONY: public


### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)


### OCI BUILD IMAGE RELATED

build-image: ## Create an OCI image with build environment (to be used in CI)
build-image: flake.nix
build-image: flake.lock
build-image: rust-toolchain.toml
build-image:
	nix build \
		--print-build-logs \
		.#docker-image
	cp --dereference result $@

load-build-image: ## Load the OCI image using Podman
load-build-image: build-image
	gzip --decompress $^ --to-stdout | podman image load
.PHONY: load-build-image

build-inside-container: ## Build the program in an OCI container (e.g. to test CI)
build-inside-container: load-build-image
	# We need to know the project name to know which image to load. Can we do better?
	test $${project_name}
	podman run \
		--rm \
		--interactive \
		--tty \
		--volume ${PWD}:/src \
		--workdir /src \
		--entrypoint make \
		localhost/$(project_name)-build-image \
		public
.PHONY: build-inside-container
