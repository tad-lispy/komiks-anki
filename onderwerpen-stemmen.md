# ![Mazanki.com Onderwerpen stemmen](assets/mazanki-logo.svg)

# Onderwerpen stemmen

Welke onderwerpen zou jij graag op Mazanki.app willen zien?

Zet een plusteken (+) naast elk onderwerp dat je interessant vindt.

- 0 De stad en richting

- 0 Kinderopvang en school

- 0 Vervoer (trein, fiets, auto, vliegtuig)

- 0 Weer

- 1 Dieten, eten en drinken

- 1 Gemeentehuis (trouwen, rijbewijs, geboorteaangifte, paspoort)

- 1 Hobby's

- 1 In de bibliotheek

- 1 Kleding

- 1 Landen in Europa

- 1 Spelen

- 1 Sport

- 2 Het lichaam

- 2 Keuken en koken

- 2 Omgangsvormen

- 2 Tijd

- 3 Beroepen

- 3 Bezoek aan de dokter, tandarts en apotheek

- 3 Familie

- 3 Feestdagen

- 3 Huis

- 3 In de winkel (bakker, drogisterij, groentewinkel, kruidenierswinkel, parfumerie)

- 3 Studeren, opleiding en werk in Nederland

- Anders:

- Anders:

- Anders: 


Wil je helpen? Schrijf uw naam en e-mail of telefoonnummer.
